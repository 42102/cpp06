
#include "../inc/Utils.hpp"

Base * generate (void)
{
	unsigned int first_interval = 0, second_interval = 0,
				 third_interval = 0;
	unsigned int number;
	srand(std::time(NULL));

	for(unsigned int i = 0; i < 100; i++)
	{
		number = rand() % 99;
		if(number <= 32)
			first_interval++;
		else if(number <= 65)
			second_interval++;
		else
			third_interval++;
	}

	if(first_interval >= second_interval && first_interval >= third_interval)
		return new A();
	else if(second_interval >= first_interval && second_interval >= third_interval)
		return new B();
	else if(third_interval >= first_interval && third_interval >= second_interval)
		return new C();
	return NULL;
}

void identify (Base *p)
{
	if(dynamic_cast<A *> (p) != NULL)
		std::cout<<"Class corresponds to: A"<<std::endl;
	else if(dynamic_cast<B *> (p) != NULL)
		std::cout<<"Class corresponds to: B"<<std::endl;
	else if(dynamic_cast<C *> (p) != NULL)
		std::cout<<"Class corresponds to: C"<<std::endl;
	else
		std::cout<<"Class not corresponds to any other"<<std::endl;
}
