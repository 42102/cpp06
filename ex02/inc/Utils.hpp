
#ifndef UTILS_HPP_
#define UTILS_HPP_

#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

Base *generate (void);
void identify (Base *p);

#endif
