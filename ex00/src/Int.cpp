
#include <iostream>

char intToChar (int integer)
{
	char c = '\0';
	if(integer > 126)
	{
		std::cout<<"Impossible";
		return c;
	}
	if(integer < 32)
		std::cout<<"Non displayable";
	else	
		c = static_cast<char > (integer);
	return c;
}

float intToFloat (int integer)
{

	float f = static_cast<float> (integer);
	return f;
}

double intToDouble (int integer)
{
	double d = static_cast<double> (integer);
	return d;
}
