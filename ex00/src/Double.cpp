
#include <iostream>
#include <limits>

char doubleToChar (double d)
{
	char c = '\0';

	if(d < static_cast<double>(127) && d >= static_cast<double> (32))
	{
		c = static_cast<char> (d);	
	}
	else
	{
		std::cout<<"Impossible";
	}

	return c;
}

void doubleToInt (double d)
{
	int integer;

	if (static_cast<double> (2147483648) > d && static_cast<double> (-2147483649) < d)
	{
		integer = static_cast<double> (d);
		std::cout<<integer;
	}
	else
	{
		std::cout<<"Impossible";
	}
}

void doubleToFloat (double d)
{
	float fl;

	const float FLT_MAX = std::numeric_limits<float>::max();

	if(d <= static_cast<double> (FLT_MAX) && d >= static_cast<double> (-FLT_MAX))
	{
		fl = static_cast<float> (d);
		std::cout<<fl<<"f";
	}
	else
	{
		std::cout<<"Impossible";
	}
}
