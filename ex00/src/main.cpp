
#include "../inc/Conversion.hpp"
#include <iostream>

int main(int argc, char *argv[])
{
	if(argc == 2)
	{
		std::string str(argv[1]);
		conversion(str);
	}
	else
	{
		std::cout<<"You should introduce 1 parameter"<<std::endl;
	}
	return 0;
}
