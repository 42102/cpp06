
#include "../inc/Conversion.hpp"

//Auxiliar methods declaration
bool checkChar (std::string &str);
bool checkInt (std::string const &str);
bool checkFloat (std::string const &str);
bool checkDouble (std::string const &str);
bool checkPseudoLiteralsFloat (std::string const &str);
bool checkPseudoLiteralsDouble (std::string const &str);

//Methods implementation of Conversion to treat the string
void conversion(std::string &str)
{
	switch(parseStr(str))
	{
		case 1:
			std::cout<<BLUE<<"La cadena introducida es un Char: "
				<<"'"<<str<<"'"<<";"<<RESET<<std::endl;
			convertChar(str[0]);
			break;
        case 2:
            std::cout<<ORANGE<<"La cadena introducida es un Int: "
				<<str<<";"<<RESET<<std::endl;
        	convertInt(atoi(str.c_str()));
			break;
		case 3:
			std::cout<<GREEN<<"La cadena introducida es un Float: "
				<<str<<";"<<RESET<<std::endl;
			convertFloat(str);
			break;
		case 4:
			std::cout<<MAGENTA<<"La cadena introducida es un Double: "
				<<str<<";"<<RESET<<std::endl;
			convertDouble(str);
			break;
		default:
			std::cout<<RED<<"It is not a valid str: "<<str<<RESET<<std::endl;
	}
}

int parseStr(std::string &str)
{
	unsigned int pos = 0;

	while(str[pos] == ' ' && str[pos] != '\0')
		pos++;
    
    if(str[pos] == '\0' && pos == 1)
    {   
        return 1;
    }
	if(str[pos] == '\0')
	{
		std::cout<<"Error: Parameter full of white spaces!!!"<<std::endl;
		//Return value of -1 means full of white spaces
		return -1;
	}
	
	str = str.substr(pos, str.length() - pos);
	
    //return values 1->char, 2->int, 3->float, 4->double, 0->invalid string
	if(checkChar(str))
		return 1;
	else if(checkInt(str))
        return 2;
	else if(checkFloat(str))
		return 3;
	else if(checkDouble(str))
		return 4;
	else
		std::cout<<RED<<"Error: Introduced parameter it is not correct!!!"<<RESET<<std::endl;
	return 0;
}

//Auxiliar methods to check if string passed corresponds with anyone
bool checkChar (std::string &str)
{
	unsigned int pos = 0;
	unsigned int chars_founded = 0;
    unsigned int numbers = 0;

	while(str[pos] != '\0')
	{
		if((str[pos] >= '!' && str[pos] <= '/') || (str[pos] >= ':' && str[pos] <= '~'))
			chars_founded++;
        if(str[pos] >= '0' && str[pos] <= '9')
            numbers++;
		pos++;
	}
    
	if(chars_founded != 1)
		return false;
    if(numbers > 0)
        return false;
    str = str.substr(0, 1);
	return true;	
}

bool checkInt(std::string const &str)
{
    unsigned int sign = 0;
    unsigned int pos = 0;
	long number;

    while(str[pos] != '\0')
    {
        if(str[pos] == '+' || str[pos] == '-')
        {
            sign++;
            pos++;
            continue;
        }
        if(sign > 1)
            return false;
        if(str[pos] < '0' ||  str[pos] > '9')
            return false;
        pos++;
    }

    if(str[pos] == '\0')
	{
		try
		{
			number = stoll(str);
			//The following statement will check the range of int
			if(number >= -2147483648 && number <= 2147483647)
				return true;
			else
				return false;
		}
		catch(std::exception &e)
		{
			std::cout<<RED<<e.what()<<RESET<<std::endl;
			return false;
		}
	}
    else
        return false;
}

bool checkFloat(std::string const &str)
{
    unsigned int sign = 0;
    unsigned int pos = 0;
	bool number = false;
	bool dot = false;
	if(checkPseudoLiteralsFloat(str))
		return true;

    while(str[pos] != '\0')
    {

        if(str[pos] == '+' || str[pos] == '-')
        {
            sign++;
            pos++;
            continue;
        }
		if(dot == true && str[pos] == '.')
			return false;
		if(str[pos] == '.' && number == true)
		{
			
        	if(str[pos + 1] < '0' ||  str[pos + 1] > '9')
				return false;
			dot = true;
			pos++;
			continue;
		}
        if(sign > 1)
            return false;
		if(number == true && dot == true && str[pos] == 'f')
			break;
        if(str[pos] < '0' ||  str[pos] > '9')
            return false;
		else
			number = true;
        pos++;
    }

	if(str[pos] == 'f' && str[pos + 1] == '\0' && dot == true)
		try
		{
			float f = stof(str, NULL);
			(void)f;
			return true;
		}catch(std::exception &e)
		{
			std::cout<<RED<<e.what()<<RESET<<std::endl;
			return false;
		}
	else
		return false;
}


bool checkDouble (std::string const &str)
{
	
    unsigned int sign = 0;
    unsigned int pos = 0;
	bool number = false;
	bool dot = false;
	
	if(checkPseudoLiteralsDouble(str))
		return true;
    while(str[pos] != '\0')
    {

        if(str[pos] == '+' || str[pos] == '-')
        {
            sign++;
            pos++;
            continue;
        }
		if(dot == true && str[pos] == '.')
			return false;
		if(str[pos] == '.' && number == true)
		{
			dot = true;
			pos++;
			continue;
		}
        if(sign > 1)
            return false;
        
		if(str[pos] < '0' ||  str[pos] > '9')
            return false;
		else
			number = true;
        pos++;
    }
	if(str[pos - 1] == '.' && str[pos] == '\0')
		return false;
	if(str[pos] == '\0' && dot == true)
	{

		try
		{
			double  d = stod(str, NULL);
			(void)d;
			return true;
		}catch(std::exception &e)
		{
			std::cout<<RED<<e.what()<<RESET<<std::endl;
			return false;
		}
		return true;
	
	}
	else
	{
		return false;
	}
}

bool checkPseudoLiteralsFloat (std::string const &str)
{
	if(str.compare("nanf") == 0 || str.compare("+inff") == 0  || str.compare("-inff") == 0)
		return true;

	return false;
}

bool checkPseudoLiteralsDouble (std::string const &str)
{	
	if(str.compare("nan") == 0 || str.compare("+inf") == 0  || str.compare("-inf") == 0)
		return true;
	return false;
}
//-----------------------------------------------------------------------------
//Functions to make conversions

void convertChar(char c)
{

	std::cout<<CYAN<<"-------------------------------"<<RESET<<std::endl;
	std::cout<<BLUE<<"Char: '"<<c<<"'"<<RESET<<std::endl;
	std::cout<<ORANGE<<"Int: "<<charToInt(c)<<RESET<<std::endl;
	std::cout<<GREEN<<std::setprecision(1)<<std::fixed<<
		"Float: "<<charToFloat(c)<<"f"<<RESET<<std::endl;
	std::cout<<MAGENTA<<"DOUBLE: "<<charToDouble(c)<<RESET<<std::endl;

}

void convertInt (int integer)
{
	//If we reach this points integer is inside range
	std::cout<<CYAN<<"-------------------------------"<<RESET<<std::endl;
	std::cout<<BLUE<<"Char: '"<<intToChar(integer)<<"'"<<RESET<<std::endl;
	std::cout<<ORANGE<<"Int: "<<integer<<RESET<<std::endl;
	std::cout<<GREEN<<std::setprecision(1)<<std::fixed
		<<"Float: "<<intToFloat(integer)<<"f"<<RESET<<std::endl;	
	std::cout<<MAGENTA<<"DOUBLE: "<<intToDouble(integer)<<RESET<<std::endl;
}

void convertFloat (std::string str)
{
	if(checkPseudoLiteralsFloat(str))
	{
		std::cout<<CYAN<<"-------------------------------"<<RESET<<std::endl;	
		std::cout<<BLUE<<"Char: '"<<"Impossible"<<"'"<<RESET<<std::endl;
		std::cout<<ORANGE<<"Int: "<<"Impossible"<<RESET<<std::endl;
		std::cout<<GREEN<<"Float: "<<str<<RESET<<std::endl;	
		std::cout<<MAGENTA<<"DOUBLE: "<<str.substr(0, str.length() - 1)<<RESET<<std::endl;
	}
	else
	{
		try
		{
			float f = stof(str);
			
			std::cout<<CYAN<<"-------------------------------"<<RESET<<std::endl;	
			std::cout<<BLUE<<"Char: '"<<floatToChar(f)<<"'"<<RESET<<std::endl;
			std::cout<<ORANGE<<"Integer: ";
			floatToInt(f);
			std::cout<<RESET<<std::endl;
			std::cout<<std::fixed<<std::setprecision(1)<<GREEN<<"Float: "<<f<<"f"<<RESET<<std::endl;	
			std::cout<<MAGENTA<<"Double: "<<floatToDouble(f)<<RESET<<std::endl;
		}
		catch (std::exception &e)
		{
			std::cout<<RED<<e.what()<<RESET<<std::endl;
		}
	}
}

void convertDouble(std::string str)
{
	if(checkPseudoLiteralsDouble(str))
	{	
		std::cout<<CYAN<<"-------------------------------"<<RESET<<std::endl;	
		std::cout<<BLUE<<"Char: '"<<"Impossible"<<"'"<<RESET<<std::endl;
		std::cout<<ORANGE<<"Int: "<<"Impossible"<<RESET<<std::endl;
		std::cout<<GREEN<<"Float: "<<str<<"f"<<RESET<<std::endl;	
		std::cout<<MAGENTA<<"DOUBLE: "<<str<<RESET<<std::endl;
	}
	else
	{	
		std::cout<<CYAN<<"-------------------------------"<<RESET<<std::endl;
		try
		{
			double d = stod(str);

			std::cout<<BLUE<<"Char: '"<<doubleToChar(d)<<"'"<<RESET<<std::endl;
			std::cout<<ORANGE<<"Int: ";
			doubleToInt(d);
			std::cout<<RESET<<std::endl;
			std::cout<<std::fixed<<std::setprecision(1)<<GREEN<<"Float: ";
			doubleToFloat(d);
			std::cout<<RESET<<std::endl;
			std::cout<<MAGENTA<<"Double: "<<d<<RESET<<std::endl;
		}
		catch(std::exception &e)
		{
			std::cout<<RED<<e.what()<<RESET<<std::endl;
		}
	}
}
