
#include <iostream>

int charToInt(char c)
{
	int integer = static_cast<int> (c);

	return integer;
}

float charToFloat(char c)
{
	float f = static_cast<float> (c);

	return f;
}

double charToDouble(char c)
{
	double d = static_cast<double> (c);
	return d;
}
