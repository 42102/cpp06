
#include <iostream>

char floatToChar (float fl)
{
	char c;
	
	if(fl >= (float)32 && fl < (float)127)
	{
		c = static_cast<char> (fl);
	}
	else
	{
		std::cout<<"Impossible";
		c = '\0';
	}
	return c;
}

void floatToInt (float fl)
{
	int integer;

	if(static_cast<float> (2147483648) > fl && static_cast<float> (-2147483649) < fl)
	{

		integer = static_cast<int> (fl);
		std::cout<<integer;
	}
	else
	{
		std::cout<<"Impossible";
	}
}

double floatToDouble (float fl)
{
	double d;

	d = static_cast<double> (fl);

	return d;
}
