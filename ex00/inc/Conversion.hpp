
#ifndef CONVERSION_HPP_
#define CONVERSION_HPP_

//Colors to format text
#define RED "\033[91m"
#define GREEN "\033[92m"
#define BLUE "\033[94m"
#define ORANGE "\033[93m"
#define MAGENTA "\033[95m"
#define CYAN "\033[36m"
#define RESET "\033[0m"

#include <iostream>
#include <iomanip>

//Functions to treat the string
void conversion(std::string &str);
int parseStr(std::string &str);

//Functions to make conversions

void convertChar(char c);
void convertInt(int integer);
void convertFloat(std::string str);
void convertDouble (std::string str);

//Conversion from char to other types
int charToInt(char c);
float charToFloat(char c);
double charToDouble(char c);

//Conversion from int to other types
char intToChar(int integer);
float intToFloat(int integer);
double intToDouble(int integer);

//Convert from float to other types
char floatToChar(float fl);
void floatToInt(float fl);
double floatToDouble (float fl);

//Convert from double to other types
char doubleToChar (double d);
void doubleToInt(double d);
void doubleToFloat (double d);
#endif
