
#include "../inc/Serialization.hpp"
#include <iostream>

int main (void)
{
	
	Data *ptr;
	Data *new_ptr;
	ptr = new Data;

	ptr->age = 23;
	ptr->male = true;
	ptr->name = "Juan";

	uintptr_t raw = serialize (ptr);
	new_ptr = deserialize (raw);
	showData(*ptr);
	std::cout<<"Memory address of ptr: "<<ptr<<std::endl;
	std::cout<<"raw data: "<<raw<<std::endl;
	
	showData(*new_ptr);
	delete ptr;
	
	return 0;
}
