
#include "../inc/Serialization.hpp"

uintptr_t serialize (Data *ptr)
{
	uintptr_t raw_data;

	raw_data = reinterpret_cast<uintptr_t> (ptr);

	return raw_data;
}

Data * deserialize (uintptr_t raw)
{
	Data * data;

	data = reinterpret_cast<Data *> (raw);
	
	return data;
}

void showData (const Data &d)
{
	std::cout<<"Nombre: "<<d.name<<", Age: "<<d.age<<", Male: "<<d.male<<std::endl;
}
