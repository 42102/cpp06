
#if !defined(SERIALIZATION_HPP_)
#define SERIALIZATION_HPP_
#include <iostream>

//Struct to serialize
typedef struct s_person {
	std::string name;
	unsigned int age;
	bool male ;
}Data;

//Function to serialize struct data
uintptr_t serialize(Data *ptr);

//Function to deserialize raw struct data
Data * deserialize(std::uintptr_t raw);

//Function to show struct Data
void showData(const Data &d);
#endif
